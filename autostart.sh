#! /bin/bash
picom &
nitrogen --restore &
unclutter &
nm-applet &
dwmblocks &
syncthing &
#(conky | while read LINE; do xsetroot -name "$LINE"; done) &

#while true; do
#    xsetroot -name "` date +"%b %e, %Y - %R" `"
#    sleep 1m    # Update time every minute
#done &
